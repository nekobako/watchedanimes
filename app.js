var http = require('http');
var path = require('path');
var express = require('express');
var client = require('cheerio-httpcli');

var app = express();
var server = http.Server(app);

// Express settings
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));

// Page renderers
app.get('/', function (req, res) {
	res.render('index.ejs');
});
app.get('/latest', function (req, res) {
	new Promise(resolve => {
		client.fetch('https://ja.wikipedia.org/wiki/%E6%B7%B1%E5%A4%9C%E3%82%A2%E3%83%8B%E3%83%A1%E4%B8%80%E8%A6%A7', (err, $, res, body) => {
			var animes = [];
			$('#mw-content-text > dl')
				.map((i, dl) => ({
					year: parseInt($($(dl).prevAll('h3').first().children().first()).text()),
					titles: $(dl).find('li:not(:has(ul)) > a:first-child').map((i, a) => $(a).text()).get()
				}))
				.get()
				.forEach(x => {
					var record = animes.filter(y => y.year === x.year)[0];
					if(record) {
						record.titles = record.titles.concat(x.titles);
					}
					else {
						animes.push(x);
					}
				})
			resolve(animes.filter(x => x.year >= 2000));
		});
	})
	.then(animes => {
		res.render('latest.ejs', {
			animes: animes
		});
	});
});

// Error handlers
app.use(function (req, res, next) {
	res.status(404).send('404 Not Found');
});
app.use(function (err, req, res, next) {
	res.status(500).send('500 Internal Server Error');
});

// Server listener
var port = 80;
server.listen(port, function () {
	console.log('listening on port ' + port);
});

module.exports = app;
